const Koa = require('koa')
const cors = require('@koa/cors');
const app = new Koa()
const RedisStore = require('koa-redis');

app.use(cors())
// trust proxy
app.proxy = true

// sessions
const session = require('koa-session')
app.keys = ['your-session-secret']
app.use(session({  store: new RedisStore(),}, app))

// body parser
const bodyParser = require('koa-bodyparser')
app.use(bodyParser())

// authentication
require('./auth')
const passport = require('koa-passport')
app.use(passport.initialize())
app.use(passport.session())

const jwt = require('jsonwebtoken');
const route = require('./route.js');

app.use(route.routes());


const port = process.env.PORT || 3000
app.listen(port, () => console.log('Server listening on', port))