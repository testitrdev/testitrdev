const Router = require('koa-router');
const passport = require('koa-passport');
const jwt = require('jsonwebtoken');

// const queries = require('../db/queries/users');

const router = new Router();

router.post('/auth/login', async (ctx) => {
    return passport.authenticate('local', (err, user, info, status) => {
      if (user) {
        let token = jwt.sign({
                      exp: Math.floor(Date.now() / 1000) + (60 * 60),
                      data: user.login
                    }, 'secret');
        ctx.login(user)
        ctx.body = {status: 'success', token:token, user:user.name};
      } else {
        ctx.body = { status: 'error' };
      }
    })(ctx);
  })


module.exports = router;