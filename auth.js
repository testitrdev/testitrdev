const passport = require('koa-passport')

const fetchUser = ((username) => new Promise(resolve => {
  const users = [
    { login: 'john', password: '123', name: 'John Snow' },
    { login: 'pink', password: 'qwerty', name: 'Pink Floyd' },
    { login: 'tom', password: 'admin', name: 'Tom Cruise' },
    { login: 'dogo', password: 'dogy', name: 'great dogo'},
  ]
  let user = users.filter(user=> username==user.login);
  user = user.length > 0 ? user[0] : null;
  resolve(user)
}))


passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser(async function (userData, done) {
  try {
    const user = await fetchUser(userData.login)
    done(null, user)
  } catch (err) {
    done(err)
  }
})

const LocalStrategy = require('passport-local').Strategy
passport.use(new LocalStrategy({}, (username, password, done) => {
  fetchUser(username)
    .then(user => {
      if (!user) return done(null, false);
      if (password == user.password) {
        return done(null, user);
      } else {
        return done(null, false);
      }
    })
    .catch((err) => {
      return done(err);
    });
}))